# Creating multi-page blueprint with control over the resulting structure

This repository contains example code for creating a multi-page blueprint with control over the structure.

Follow our step by step tutorial here -
[Writing a Multi-page Blueprint using Atlassian Connect](https://developer.atlassian.com/display/CONFDEV/Writing+a+Multi-page+Blueprint+using+Atlassian+Connect)